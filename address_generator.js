/*
 * For only generating address purposes
 * Can not use extended public key to sign transactions
 */

const dotenv = require('dotenv')
const hdkey = require('hdkey')
const createHash = require('create-hash')
const bs58check = require('bs58check')
const _ = require('lodash')
const RPCClient = require('./rpc_client')
const rpcClient = new RPCClient()

dotenv.config()
const PUBLIC_EXTENDED_KEY = process.env.PUBLIC_EXTENDED_KEY

class AddressGenerator {
    getAddressByUserId (userId, needImport = false) {
        return new Promise((resolve, reject) => {
            if (_.isNumber(userId)) {
                const publicNode = hdkey.fromExtendedKey(PUBLIC_EXTENDED_KEY)
                const userNode = publicNode.deriveChild(userId)
                
                // https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses
                const step1 = userNode._publicKey
                const step2 = createHash('sha256').update(step1).digest()
                const step3 = createHash('rmd160').update(step2).digest()
                var step4 = Buffer.allocUnsafe(21)
                step4.writeUInt8(0x6f, 0) // 0x00 for mainnet, 0x6f for testnet
                step3.copy(step4, 1) // step4 now holds the extended RIPEMD160 result
                var address = bs58check.encode(step4); //Steps 5-9

                if (needImport) {
                    rpcClient.callRPC('importaddress', [address, '', false])
                        .then(() => {
                            resolve(address)
                        })
                        .catch((error) => {
                            reject(error)
                        })
                } else {
                    resolve(address)
                }
            } else {
                reject("User Id is not a number")
            }
        })
    }
}

module.exports = AddressGenerator
