const bip39 = require('bip39')
const hdkey= require('hdkey')
const fs = require('fs')

const passphrase = 'password'

const main = async () => {
    // Bip39
    const mnemonic = bip39.generateMnemonic()
    const seed = await bip39.mnemonicToSeed(mnemonic, passphrase)
    
    // Hdkey
    const root = hdkey.fromMasterSeed(seed) 
    const node = root.derive("m/44'/0'/0'/0") // bitcoin testnet
        
    const publicExtendedKey = node.publicExtendedKey.toString('hex')    
    const privateExtendedKey = node.privateExtendedKey.toString('hex')
    console.log(`Mnemonic ${mnemonic}\nPublic Extended Key: ${publicExtendedKey}\nPrivate Extended Key: ${privateExtendedKey}`)
    fs.writeFile('wallet.txt', `Mnemonic ${mnemonic}\nPublic Extended Key: ${publicExtendedKey}\nPrivate Extended Key: ${privateExtendedKey}`,
        (error) => {
            if (error) console.log(error)
            console.log('Saved!')
        }
    ) 
}

main()
