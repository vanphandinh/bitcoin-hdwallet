const AddressGenerator = require('./address_generator')
const addressGenerator = new AddressGenerator()
const WalletManager = require('./wallet-manager')
const walletManager = new WalletManager()

const main = async () => {
    let addresses = []
    // test genrerating address
    for (let i = 0; i < 2; i++) {
        const address = await addressGenerator.getAddressByUserId(i, true)
        const balance = await walletManager.getBalance(address)
        addresses.push(address)
        console.log(`Address: ${address}\nBalance: ${balance}\n`)
    }
    
    // test fundTxInput
    // walletManager.fundTxInput(addresses[0], 0.0005, 0.0005)
    //     .then(console.log)

    // test getTransaction
    // walletManager.getTransaction('18915502f649e9ede1228f6787821ee15ac90469fe1d19194b94f8f9f7aed0bd')
    //     .then(console.log)
    
    const user = {
        id: 0,
        address: addresses[0]
    }
    walletManager.sendBTC(user, addresses[1], 0.005, 0.0001)
        .then(console.log)
}

main()
