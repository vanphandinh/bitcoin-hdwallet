const axios = require('axios')
const dotenv = require('dotenv')

dotenv.config()
const BTC_NODE = process.env.BTC_NODE
const RPC_USER = process.env.RPC_USER
const RPC_PASSWORD = process.env.RPC_PASSWORD

class RPCClient {
    constructor() {
        this.axiosInstance = axios.create({
            timeout: 30000,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Basic ${Buffer.from(`${RPC_USER}:${RPC_PASSWORD}`).toString('base64')}`
            },
        })
    }

    async callRPC(method, params) {
        const data = {
            jsonrpc: '2.0',
            method,
            params,
            id: 1
        }
        const response = await this.axiosInstance.post(BTC_NODE, data)
        return response.data.result
    }
}

module.exports = RPCClient
