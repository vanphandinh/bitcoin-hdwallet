/* 
 * For generating withdraw transactions
 * Can use private extended key to generate address and sign transaction
 */

const dotenv = require('dotenv')
const bs58check = require('bs58check')
const _ = require('lodash')
const hdkey = require('hdkey')
const bitcore = require('bitcore-lib')
const RPCClient = require('./rpc_client')
const rpcClient = new RPCClient()

dotenv.config()
const PRIVATE_EXTENDED_KEY = process.env.PRIVATE_EXTENDED_KEY

class WalletManager {
    constructor() {
        this.lastTxInputs = []
    }

    // user = {
    //     id: Number,
    //     address: String
    // }
    async sendBTC(fromUser, toAddress, amount, fee) {
        if (!fromUser.id && !fromUser.address) throw new Error('User is not found') 
        // TODO: Validating works

        const privateNode = hdkey.fromExtendedKey(PRIVATE_EXTENDED_KEY)  
        const userNode = privateNode.deriveChild(fromUser.id)

        const privateKey = new bitcore.PrivateKey(userNode._privateKey.toString('hex')) 
        const inputs = await this.fundTxInput(fromUser.address, amount, fee)
        if (_.isEmpty(inputs)) return

        const serializeTx = new bitcore.Transaction()
            .from(inputs)
            .to(toAddress, this.toSatoshi(amount))
            .change(fromUser.address)
            .fee(this.toSatoshi(fee))
            .sign(privateKey)
            .serialize()
        const rawTx = serializeTx.toString('hex')
        
        return rpcClient.callRPC('sendrawtransaction', [rawTx]) 
    }

    async getBalance(address) {
        const utxos = await this.unspentByAddress(address)
        const amount = _.sumBy(utxos, (utxo) => utxo.amount).toFixed(8) 
        return amount
    }

    async getTransaction(txHash) {
        const result = await rpcClient.callRPC('gettransaction', [txHash, true])
        return result
    }

    async fundTxInput(sender, amount, fee) {
        const utxos = await this.unspentByAddress(sender)
        let inputs = []
        let totalAmount = 0
        for (let i = 0; i < utxos.length; i++) {
            inputs.push ({
                txid: utxos[i].txid,
                vout: utxos[i].vout,
                address: utxos[i].address,
                scriptPubKey: utxos[i].scriptPubKey,
                satoshis: this.toSatoshi(utxos[i].amount)
            })
            totalAmount += utxos[i].amount
            this.pushLastTxInputs({ txid: utxos[i].txid })
            if (totalAmount >= amount + fee) return inputs 
        }
        this.removeLastTxInputs(inputs)
        return []
    }

    async unspentByAddress(address, minConf = 1, maxConf = 999999) {
        const utxos = await rpcClient.callRPC('listunspent', [minConf, maxConf, [address]])
        return _.sortBy(utxos, 'amount', ['asc'])
    }
    
    removeLastTxInputs(...inputs) {
        inputs.forEach(element => {
            this.lastTxInputs = _.remove(this.lastTxInputs, (input) => {
                return element.txid == input.txid
            })
        })
    }

    pushLastTxInputs(...inputs) {
        inputs.forEach(element => {
            this.lastTxInputs.push(element)
        })
    }

    // Buffer private key
    hexToWIF(privateKey) {
        // https://en.bitcoin.it/wiki/Wallet_import_format
        const step1 = privateKey
        console.log(step1.toString('hex'))
        const step2 = Buffer.allocUnsafe(34) 
        step2.writeUInt8(0xef, 0) // 0x80 for mainnet, 0xef for testnet
        step1.copy(step2, 1)
        step2.writeUInt8(0x01, 33) // 0x01 at the end for compressed public key
        console.log(step2.toString('hex'))
        const wif = bs58check.encode(step2) // step 3-7
        return wif 
    }

    toSatoshi(amount) {
        return Number((amount * 1e8).toFixed(0))
    }
}

module.exports = WalletManager
